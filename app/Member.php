<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Member extends Model
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     * 
     * @var string
     */

     protected $table = 'members';
     protected $primaryKey = 'id';
     protected $fillable = ['name', 'phone', 'shortBoard', 'longBoard', 'enrollment'];
}
