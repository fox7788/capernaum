<?php

namespace Tests\Unit;

use Tests\TestCase;

class MemberEditTest extends TestCase
{

    public function testUserCanViewAFormWhenAutheniated()
    {
       $user = factory('App\User')->make();
       $response = $this->actingAs($user)->get('/edit');
       $response->assertViewIs('member.edit');
    }

    public function testUserCannotViewAFormWhenNotAutheniated()
    {
        $response = $this->get('/edit');
        $response->assertRedirect('/login');
    }

    public function testUserCanSubmitFormData()
    {
        $user = factory('App\User')->make();

        $newMember = $this->getMemberData();
        $response = $this->actingAs($user)->post(
            route('memberEdit'),
            $newMember['first']
        );

        $response->assertStatus(200);
    }

    public function testUserSubmitOnlyNamePhoneEnrollment()
    {
        $user = factory('App\User')->make();

        $newMember = $this->getMemberData();
        $response = $this->actingAs($user)->post(
            route('memberEdit'),
            $newMember['second']
        );

        $response->assertStatus(200);
    }

    public function testUserSubmitDuplicatedPhone()
    {
        $user = factory('App\User')->make();

        $newMember = $this->getMemberData();
        $response = $this->actingAs($user)->post(
            route('memberEdit'),
            $newMember['duplicatePhone']
        );

        $response->assertStatus(302);
    }

    public function testUserMissInputName()
    {
        $user = factory('App\User')->make();

        $newMember = $this->getMemberData();
        $response = $this->actingAs($user)->post(
            route('memberEdit'),
            $newMember['missName']
        );

        $response->assertStatus(302);
    }

    public function testUserMissInputPhone()
    {
        $user = factory('App\User')->make();

        $newMember = $this->getMemberData();
        $response = $this->actingAs($user)->post(
            route('memberEdit'),
            $newMember['missPhone']
        );

        $response->assertStatus(302);
    }

    public function testUserMissInputEnrollment()
    {
        $user = factory('App\User')->make();

        $newMember = $this->getMemberData();
        $response = $this->actingAs($user)->post(
            route('memberEdit'),
            $newMember['missEnrollment']
        );

        $response->assertStatus(302);
    }

    public function testAjaxCheckPhone()
    {
        $user = factory('App\User')->make();
        
        $newMember = $this->getMemberData();
        $response = $this->actingAs($user)->json(
            'POST',
            route('ajaxCheckPhone'),
            [
                'phone' => $newMember['first']['phone'],
                'userId' => 1,
            ]
        );

        $response->
        assertStatus(200)->
        assertJson([
            'errCode' => 0,
            'errMsg' => '',
        ]);
    }

    private function getMemberData()
    {
        return [
            'first' =>
            [
                'name' => 'fox',
                'phone' => '0912345678',
                'shortBoard' => 1,
                'longBoard' => 0,
                'enrollment' => '2019-10-03',
            ],
            'second' => 
            [
                'name' => 'fox2',
                'phone' => '0922345678',
                'enrollment' => '2019-11-11',
            ],
            'duplicatePhone' =>
            [
                'name' => 'fox3',
                'phone' => '0922345678',
                'shortBoard' => 1,
                'longBoard' => 1,
                'enrollment' => '2019-10-10',
            ],
            'missName' =>
            [
                'phone' => '0932345678',
                'enrollment' => '2019-10-10',
            ],
            'missPhone' =>
            [
                'name' => 'fox3',
                'enrollment' => '2019-10-10',
            ],
            'missEnrollment' =>
            [
                'name' => 'fox4',
                'phone' => '0942345678',
            ]
        ];
    }
}
