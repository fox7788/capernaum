<?php

namespace Tests\Feature\Auth;

use Tests\TestCase;

class LoginTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    public function testUserCanViewALoginForm()
    {
        $response = $this->get('/login');
        
        $response->assertSuccessful();
        $response->assertViewIs('auth.login');
    }

    public function testUserCannotViewALoginFormWhenAutheniated()
    {
        $user = factory('App\User')->make();
        $response = $this->actingAs($user)->get('/login');
        $response->assertRedirect('/home');
    }

    public function testUserCanLoginWithCorrectCredentials()
    {
        $user = factory('App\User')->create([
            'email' => 'test1@fox.com',
            'password' => bcrypt($password = '1qaz2wsx'),
        ]);

        $response = $this->post('/login', [
            'email' => $user->email,
            'password' => $password,
        ]);

        $response->assertRedirect('/home');
        $this->assertAuthenticatedAs($user);
    }

    public function testUserCannotLoginWithIncorrectPassword()
    {
        $user = factory('App\User')->create([
            'email' => 'test2@fox.com',
            'password' => bcrypt('1qaz2wsx'),
        ]);

        $response = $this->from('/login')->post('/login', [
            'email' => $user->email,
            'password' => '5566',
        ]);

        $response->assertRedirect('/login');
        $response->assertSessionHasErrors('email');
        $this->assertTrue(session()->hasOldInput('email'));
        $this->assertFalse(session()->hasOldInput('password'));
        $this->assertGuest();
    }

    public function testUserCannotLoginWithIncorrectEmail()
    {
        $user = factory('App\User')->make();

        $response = $this->from('/login')->post('/login', [
            'email' => $user->email,
            'password' => $user->password,
        ]);

        $response->assertRedirect('/login');
        $response->assertSessionHasErrors('email');
        $this->assertTrue(session()->hasOldInput('email'));
        $this->assertFalse(session()->hasOldInput('password'));
        $this->assertGuest();

    }

    public function testRememberMeFuctionality()
    {
        $user = factory('App\User')->create([
            'email' => 'test3@fox.com',
            'password' => bcrypt($password = '1qaz2wsx'),
        ]);

        $response = $this->post('/login', [
            'email' => $user->email,
            'password' => $password,
            'remember' => 'on',
        ]);

        $response->assertRedirect('/home');
        $this->assertAuthenticatedAs($user);
        $response->assertCookie(\Auth::guard()->getRecallerName(), vsprintf('%s|%s|%s', [
            $user->id,
            $user->getRememberToken(),
            $user->password,
        ]));
    }

    protected function tearDown(): void
    {
        \App\User::where('email', 'test1@fox.com')->delete();
        \App\User::where('email', 'test2@fox.com')->delete();
        \App\User::where('email', 'test3@fox.com')->delete();
        parent::tearDown();
    }
}
