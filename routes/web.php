<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Authentication Routes...
//Auth::routes();
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('home', 'HomeController@index')->name('home');

Route::get('edit', 'MemberController@edit')->name('memberEdit');
Route::get('edit/{id}', 'MemberController@edit');
Route::post('ajaxCheckPhone', 'MemberController@ajaxCheckPhone')->name('ajaxCheckPhone');
Route::post('edit', 'MemberController@submit');

Route::get('list/{page}', 'MemberController@list')->name('memberList');