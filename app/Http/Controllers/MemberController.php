<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MemberController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function edit(int $id = 0)
    {
        $result = \App\Member::find($id);

        if (is_null($result)) {
            $result = [];
        }

        return view('member.edit', $result);
    }

    public function submit(Request $request)
    {
        $this->validateForm($request);

        if ($request->input('userId')) {
            list($code, $msg) = $this->updateUser($request);
        } else {
            list($code, $msg) = $this->checkAndInsert($request);
        }

        return response()->
            view('member.edit', ["message" => $msg], $code);
    }

    public function ajaxCheckPhone(Request $request) {
        $query = \App\Member::where('phone', $request->input('phone'));
        if ($request->input('userId')) {
            $query->where('id', '!=', $request->input('userId'));
        }
        $resultCount = $query->count();

        return response()->json([
            'errCode' => 0,
            'errMsg' => '',
            'result' => ['resultCount' => $resultCount]
        ]);
    }

    protected function updateUser(Request $request)
    {
        $code = 200;
        $msg = '';

        try {
            $member = \App\Member::find($request->input('userId'));
            if ($member) {
                $member->name = $request->input('name');
                $member->phone = $request->input('phone');
                $member->shortBoard = ($request->input('shortBoard') ?? 0);
                $member->longBoard = ($request->input('longBoard') ?? 0);
                $member->enrollment = $request->input('enrollment');
                $member->save();
            } else {
                $code = 302;
                $msg = 'user id not found.';
            }
        } catch (Exception $e) {
            $code = 302;
            $msg = 'user id not found.';
        }

        return [$code, $msg];
    }

    protected function checkAndInsert(Request $request)
    {
        $code = 200;
        $msg = '';

        $resultCount = \App\Member::where('phone', $request->input('phone'))->count();
        if ($resultCount == 0) {
            $member = new \App\Member;

            $member->name = $request->input('name');
            $member->phone = $request->input('phone');
            $member->shortBoard = ($request->input('shortBoard') ?? 0);
            $member->longBoard = ($request->input('longBoard') ?? 0);
            $member->enrollment = $request->input('enrollment');

            $member->save();
        } else {
            $code = 302;
            $msg = 'phone number is exists.';
        }

        return [$code, $msg];
    }

    protected function validateForm(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'phone' => 'required|string',
            'enrollment' => 'required|date',
        ],[
            'required' => 'The :attribute field is required.'
        ]);
    }
}
