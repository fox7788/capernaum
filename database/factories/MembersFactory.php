<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Member;
use Faker\Generator as Faker;

$factory->define(Member::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'phone' => $faker->unique()->phoneNumber,
        'shortBoard' => rand(0,3),
        'longBoard' => rand(0,3),
        'enrollment' => $faker->date('Y-m-d', 'now'),
    ];
});
