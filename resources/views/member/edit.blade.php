@extends('layouts.app')
@section('content')
<script>
    $(document).ready(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#phone').focusout(function() {
            $.post(
                "{{ route('ajaxCheckPhone') }}", {
                    phone: $(this).val(),
                    userId: $("#userId").val()
                },
                function(data) {
                    if (data.errCode == 0) {
                        if (data.result.resultCount != 0) {
                            $("#ajaxMsg").css("color", "red").html("電話已經重複");
                        } else {
                            $("#ajaxMsg").css("color", "green").html("✔");
                        }
                    } else {
                        alert(data.errMsg);
                    }
                }
            );
        });
    });
</script>

<div class="container">
    <div class="row justify-content-center" style="color:red">
        @if ($errors->any())
            {{ implode('', $errors->all(':message')) }}
        @elseif (isset($message))
            {{ $message }}
        @endif
    </div>
    <div class="row justify-content-center">
        <div class="card">
            <div class="card-header">
                會員管理
            </div>
            <form action="{{ route('memberEdit') }}" method="post" class="form-horizontal">
                @csrf
                <div class="card-body card-block">
                    <div class="row form-group">
                        <div class="col col-sm-5">
                            <label for="input-normal" class=" form-control-label">名字</label>
                        </div>
                        <div class="col col-sm-6">
                            <input type="text" id="input-normal" name="name" placeholder="Name" class="form-control" value="{{$name ?? ''}}">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-sm-5">
                            <label for="input-normal" class=" form-control-label">電話</label>
                        </div>
                        <div class="col col-sm-6">
                            <input type="text" id="phone" name="phone" placeholder="Phone" class="form-control" value="{{$phone ?? ''}}">
                            <span id="ajaxMsg"></span>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-sm-5">
                            <label for="input-normal" class=" form-control-label">入會日</label>
                        </div>
                        <div class="col col-sm-6">
                            <input type="text" id="input-normal" name="enrollment" placeholder="2000-01-01" class="form-control" value="{{$enrollment ?? ''}}">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-sm-5">
                            <label for="input-normal" class=" form-control-label">長板</label>
                        </div>
                        <div class="col col-sm-6">
                            <input type="text" id="input-normal" name="longBoard" placeholder="0000" class="form-control" value="{{$longBoard ?? ''}}">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-sm-5">
                            <label for="input-normal" class=" form-control-label">短板</label>
                        </div>
                        <div class="col col-sm-6">
                            <input type="text" id="input-normal" name="shortBoard" placeholder="0000" class="form-control" value="{{$shortBoard ?? ''}}">
                        </div>
                    </div>
                    @if (isset($id))
                    <input type="hidden" name="userId" id="userId" value="{{$id}}">
                    @endif
                </div>
                <div class="card-footer">
                    <input type="submit" name="submit" value="送出" class="btn btn-primary btn-sm" />
                    <input type="button" name="cancel" value="取消" class="btn btn-danger btn-sm" onClick="window.location='/home';" />
                </div>
            </form>
        </div>
    </div>
</div>
@endsection